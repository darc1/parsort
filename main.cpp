/*
 * Hw 01, merge sort code borrowed from: https://www.geeksforgeeks.org/iterative-merge-sort/
 */

#include <iostream>
#include<stdlib.h>
#include<stdio.h>
#include <fstream>
#include <vector>
#include <inttypes.h>
#include <chrono>
#include <thread>
#include <omp.h>

#define DEBUG false
#define DEBUG_MERGE false
#define DEBUG_THREAD false
#define PRINT_RESULT true
#define WRITE_RESULT false
#define DEBUG_PRESORT false

#define MIN_PARALLEL_CHUNK 1024
#define PARALLEL_PRESORT_CHUNK 1024

int binarySearch(std::vector<uint64_t>  const &arr, int left_idx, int right_index, uint64_t value, bool *found);
void merge3(std::vector<uint64_t>  &arr, int left, int mid, int right, int parallel_level);
void binSearchMerge(std::vector<uint64_t> const &arr, std::vector<uint64_t>  &tmp, int searched_start_idx, int searched_end_idx, int merged_start_idx, int chunk_start, int chunk_end);
void quickSortIterative (std::vector<uint64_t> &arr, int low_idx, int high_idx);
void printArray(std::vector<uint64_t> arr, int size);
void sortChunks(std::vector<uint64_t> &arr, int parallel_level, int n);

void parallelCopy(std::vector<uint64_t> &arr, const std::vector<uint64_t> &tmp, int offset, int parallel_level);

void printArray(std::vector<uint64_t> arr, int size){
    for(int i = 0; i <size; i++){

        printf(" %" PRIu64, arr[i]);
    }

    printf("\n");
}

// A utility function to swap two elements
void swap (std::vector<uint64_t> &arr, uint64_t a, uint64_t b )
{
    uint64_t t = arr[a];
    arr[a] = arr[b];
    arr[b] = t;
}

int partition (std::vector<uint64_t> &arr, int l, int h)
{
    uint64_t x = arr[h];
    int i = (l - 1);

    for (int j = l; j <= h- 1; j++)
    {
        if (arr[j] <= x)
        {
            i++;
            swap (arr, i, j);
        }
    }
    swap (arr, i + 1, h );
    return (i + 1);
}


void quickSortIterative (std::vector<uint64_t> &arr, int low_idx, int high_idx)
{
    // Create an auxiliary stack
    uint64_t stack[ high_idx - low_idx + 1 ];

    // initialize top of stack
    int top = -1;

    // push initial values of l and h to stack
    stack[ ++top ] = low_idx;
    stack[ ++top ] = high_idx;

    // Keep popping from stack while is not empty
    while ( top >= 0 )
    {
        // Pop h and l
        high_idx = stack[ top-- ];
        low_idx = stack[ top-- ];

        // Set pivot element at its correct position
        // in sorted array
        int part = partition( arr, low_idx, high_idx );

        // If there are elements on left side of pivot,
        // then push left side to stack
        if ( part-1 > low_idx )
        {
            stack[ ++top ] = low_idx;
            stack[ ++top ] = part - 1;
        }

        // If there are elements on right side of pivot,
        // then push right side to stack
        if ( part+1 < high_idx )
        {
            stack[ ++top ] = part + 1;
            stack[ ++top ] = high_idx;
        }
    }
}

/* Iterative mergesort function to sort arr[0...n-1] */
void mergeSort(std::vector<uint64_t> &arr, int parallel_level)
{

    int n = arr.size();

    sortChunks(std::ref(arr), parallel_level, n);

    int curr_size; // For current size of subarrays to be merged
    // curr_size varies from 1 to n/2
    int left_start; // For picking starting index of left subarray
    // to be merged

// Merge subarrays in bottom up manner. First merge subarrays of
// size 1 to create sorted subarrays of size 2, then merge subarrays
// of size 2 to create sorted subarrays of size 4, and so on.
    for (curr_size=MIN_PARALLEL_CHUNK; curr_size<=n-1; curr_size = 2*curr_size)
    {
        // Pick starting point of different subarrays of current size
        for (left_start=0; left_start<n-1; left_start += 2*curr_size)
        {
            // Find ending point of left subarray. mid+1 is starting
            // point of right
            int mid = left_start + curr_size - 1;

            int right_end = std::min(left_start + 2*curr_size - 1, n-1);

            // Merge Subarrays arr[left_start...mid] & arr[mid+1...right_end]
            merge3(std::ref(arr), left_start, mid, right_end, parallel_level);
        }
    }
}

void sortChunks(std::vector<uint64_t> &arr, int parallel_level, int n) {
    int j = 0;
    std::thread threads[parallel_level];
    int num_threads = 0;
    while(j < n){

        threads[num_threads] = std::thread(quickSortIterative, ref(arr), j, std::min(j + PARALLEL_PRESORT_CHUNK - 1, n - 1));
        num_threads++;
        if(num_threads == parallel_level){
            for(int i = 0; i < num_threads; i++){
                threads[i].join();
            }
            num_threads = 0;
        }
        j += PARALLEL_PRESORT_CHUNK;
    }

    if(num_threads > 0){
        for(int i = 0; i < num_threads; i++){
            threads[i].join();
        }
    }

    if(DEBUG_PRESORT){
        for(int j = 0; j < n; j+=PARALLEL_PRESORT_CHUNK){
            j = std::min(j, n - 1);
            for(int i=0; i<PARALLEL_PRESORT_CHUNK -1;i++){
                if(arr[j+i] > arr[j+i+1]){
                    printf("not sorted i:%d i+1:%d j:%d\n", i, i+1, j);
                }
            }
        }
    }
}

void binSearchMerge(std::vector<uint64_t> const &arr, std::vector<uint64_t> &tmp, int searched_start_idx, int searched_end_idx, int merged_start_idx, int chunk_start, int chunk_end){

    if(DEBUG_THREAD){
        std::cout << "Thread #" << std::this_thread::get_id() << ": on CPU " << sched_getcpu() << "\n";
    }
    if(DEBUG_MERGE){
        printf("bin search merge called with chunk start: %d, chunk end: %d, merged start: %d\n", chunk_start, chunk_end, merged_start_idx);
    }
    for(int i = chunk_start; i < chunk_end; i++){
        int arr_idx = merged_start_idx  + i;
        bool found;
        int idx = binarySearch(std::ref(arr), searched_start_idx, searched_end_idx, arr[arr_idx], &found);
        if(DEBUG_MERGE){
            printf("idx for %" PRIu64 " is: %d in tmp:%d, searched start: %d, searched end: %d, arr_id: %d\n", arr[arr_idx], idx, idx - searched_start_idx + i, searched_start_idx, searched_end_idx, arr_idx);

//            if(arr[arr_idx] == 0){
//                printf("assign 0 value is: %d in tmp:%d, searched start: %d, searched end: %d, arr_id: %d\n", idx, idx - searched_start_idx + i, searched_start_idx, searched_end_idx, arr_idx);
//            }
//
//            if(tmp[idx - searched_start_idx + i] != 0){
//                printf("reassign in tmp value idx is: %d idx in tmp:%d, searched start: %d, searched end: %d, merged start: %d, chunk start: %d, end: %d , arr_idx: %d, curr val %llu new val %llu\n",
//                       idx, idx - searched_start_idx + i, searched_start_idx, searched_end_idx,merged_start_idx, chunk_start, chunk_end,arr_idx,  tmp[idx - searched_start_idx + i], arr[arr_idx]);
//            }
        }

        tmp[idx - searched_start_idx + i] = arr[arr_idx];
    }

}

void parallelMerge(std::vector<uint64_t> &arr, std::vector<uint64_t> &tmp, int searched_start_idx, int searched_end_idx, int merged_start_idx, int merged_size, int parallel_level){


    int parallel_chunk = MIN_PARALLEL_CHUNK;
    if(merged_size/parallel_level > MIN_PARALLEL_CHUNK){
        parallel_chunk = merged_size/parallel_level;
    }

    if(DEBUG_MERGE){
        printf("parallel merge called with: searched start: %d, end: %d, merged start: %d, size: %d, parallel_level: %d , parallel chunk: %d\n",
                searched_start_idx, searched_end_idx, merged_start_idx, merged_size, parallel_level, parallel_chunk);
    }

    std::thread threads[parallel_level];
    int thread_count = 0;
    for(int i = 0; i < parallel_level; i++){
        int chunk_start =  i*parallel_chunk;
        int chunk_end = std::min(merged_size, (i + 1)*parallel_chunk);
        if(i == parallel_level - 1){
            chunk_end = merged_size;
        }

        if(DEBUG_MERGE){
            printf("chunk start: %d, chunk end: %d, size:%d , parallel iteration: %d\n", chunk_start, chunk_end, parallel_chunk, i);
        }
        threads[i] = std::thread(binSearchMerge, std::ref(arr), std::ref(tmp), searched_start_idx, searched_end_idx, merged_start_idx, chunk_start, chunk_end);
        thread_count++;
        if(chunk_end == merged_size){
            break;
        }
    }

    for(int i = 0; i < thread_count; i++){
        threads[i].join();
    }

}

void merge3(std::vector<uint64_t>  &arr, int left, int mid, int right, int parallel_level){

    if(mid > right || mid+1 > right) return;

    int tmp_size = right - left + 1;
    int right_size = right - mid;
    int left_size = mid - left + 1;
    std::vector<uint64_t> tmp(tmp_size, 0);

    if(DEBUG_MERGE){
        printf("tmp size: %d left: %d mid:%d, right: %d\n", tmp_size, left, mid, right );
    }

    parallelMerge(std::ref(arr), std::ref(tmp), mid + 1, right, left, left_size, parallel_level);
    parallelMerge(std::ref(arr), std::ref(tmp), left, mid, mid + 1,  right_size, parallel_level);

    parallelCopy(std::ref(arr), std::ref(tmp), left, parallel_level);
}

void copyChunk(std::vector<uint64_t> &arr, const std::vector<uint64_t> &tmp, int offset, int chunk_start, int chunk_end){
    
    for(int i = chunk_start; i< chunk_end; i++){
        
        arr[offset + i] = tmp[i];
        
    }
}

void parallelCopy(std::vector<uint64_t> &arr, const std::vector<uint64_t> &tmp, int offset, int parallel_level) {

    std::vector<std::thread> threads(parallel_level);
    unsigned long chunk_size = tmp.size()/parallel_level;
    int num_threads = 0;
    for(int i = 0; i < parallel_level; i++){

        int chunk_start = i * chunk_size;
        int chunk_end = std::min((i+1)*chunk_size, tmp.size());
        if(i == parallel_level - 1){
            chunk_end = tmp.size();
        }
        threads[i] = std::thread(copyChunk, std::ref(arr), std::ref(tmp), offset, chunk_start, chunk_end);
        num_threads++;
        if(chunk_end == tmp.size()){
            break;
        }
    }

    for(int i = 0; i < num_threads; i++){
        threads[i].join();
    }


//    for(int i = 0; i < tmp.size(); i++){
//
//        if(tmp[i] == 0){
//            if(DEBUG_MERGE){
//            printf("copy value 0, i: %d, tmp size: %d left: %d mid:%d, right: %d\n", i, tmp.size(), offset);
//            }
//        }
//        arr[offset + i] = tmp[i];
//    }
}

int binarySearch(std::vector<uint64_t>  const &arr, int left_idx, int right_index, uint64_t value, bool *found)
{
    *found = false;
    while (left_idx <= right_index)
    {
        int pivot = left_idx + (right_index-left_idx)/2;

        // Check if x is present at mid
        if (arr[pivot] == value){
            *found = true;
            return pivot;
        }

        // If x greater, ignore left half
        if (arr[pivot] < value)
            left_idx = pivot + 1;

            // If x is smaller, ignore right half
        else
            right_index = pivot - 1;
    }

    // if we reach here, then element was
    // not present
    return left_idx;
}

/* Driver program to test above functions */
int main(int argc, char *argv[])
{
    bool print_stdout = true;
    bool write_result = false;
    if(argc < 3){
        printf("not enough arguments\n");
        return 1;
    }

    if(argc > 3){
        printf("not printing to stdout\n");
        print_stdout = false;
    }

    if(argc > 4){
        printf("saving result to out.txt\n");
        write_result = true;
    }

    if(DEBUG) {
        printf("command: %s\n", argv[0]);
        printf("processors: %s\n", argv[1]);
        printf("file: %s\n", argv[2]);
    }

    std::vector<uint64_t> nums;
    std::ifstream file(argv[2]);
    std::string line;
    while (std::getline(file, line)) {
        uint64_t item = std::stoull(line);
        nums.push_back(item);
    }


    if(DEBUG){

        printf("loaded %lu elements sorting\n", nums.size());

    }
    auto start = std::chrono::high_resolution_clock::now();
    int parallel_level = std::atoi(argv[1]);
    mergeSort(std::ref(nums), parallel_level);
    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();

    printf("MERGESORT : %llu" "\n", microseconds);

    if(PRINT_RESULT && print_stdout) {
        for (int i = 0; i < nums.size(); i++) {

            printf("%" PRIu64 "\n", nums[i]);

        }
    }

    if(WRITE_RESULT || write_result){
        remove( "out.txt" );
        std::ofstream result("out.txt");
        for(int i=0; i < nums.size(); i++){

            result << std::to_string(nums[i]) << "\n";

        }
    }

}


